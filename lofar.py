# -*- coding: utf-8 -*-
"""
Created on Sun Dec  2 23:11:37 2018

@author: Faik

Python 3.7
"""

'''
RANK:
    INPUT - Path tuple e.g (5,4,3,2)
    DISTANCE - length of the tuple
    DROP - Difference between adjancent elements of the tuple.
    RETURNS TUPLE(DISTANCE,DROP)
'''
def rank(dlist):
    diff = [s - t for s, t in zip(dlist, dlist[1:])]
    drop = sum(diff)
    dist = len(dlist)
    return (dist,drop)

'''
FACTOR:
    INPUT - 4 Tuples of length two i.e (DISTANCE,DROP)
    USE: Checks whick tuple is most prefreable to keep(based on distance and speed) 
    and which tuple to discard.
    LOGIC:
        1. Finds the max of the distance of all the tuple.
        2. Finds index for the max.
        3. If number of index is not equal to 1 that would mean there are two 
        paths with same distance, to solve this we than calculate max of the 
        drop of two same length path, the one which is greater gets selected, 
        if same anyone of them can get selected.
    RETURNS INDEX(of selected tuple) 0(UP) or 1(DOWN) 0r 2(..) or 3(..)
    
'''   
def factor(u=(0,0),d=(0,0),l=(0,0),r=(0,0)):
        distance = (u[0],d[0],l[0],r[0])
        drop = (u[1],d[1],l[1],r[1])
        
        max_dist = max(distance)        
        index_dist = [i for i, e in enumerate(distance) if e == max_dist]
        
        if len(index_dist) == 1:
            selected = index_dist[0]
        else:
            max_drop = max([drop[i] for i in index_dist])
            index_drop = drop.index(max_drop)
            selected = index_drop
        
        return selected

'''
ITEM-CLASS:
    This is the class for numbers.
    Contains 1 Constructor, 7 Functions and 2 Refrenced Functions.
'''    
class item_class:    
    
    '''
    CONSTRUCTOR:
        self.item - number
        self.key - location e.g (1,0)(0,0) in (row,col) format
        self.size - size of the matrix (rowSize,colSize)
        self.localList - Prefered path - 
                         0 means down
                         1 means up
                         2 means left
                         3 means right
                         4 means self
                         
        self.neighbour - neighbour's item_class objects
        
        self.ignore_flags - Ignoring double cheking when it's neighbour has been checked.
                            for e.g if we have 8 9 3 2 and we are checking 8, 8's right 
                            neighbour is 9, 8 cannot go to 9, ignore_flags are updated for 9
                            that it doesn't need to check for it's left neighbour because if 8
                            cannot move to 9, the opposite will be true i.e 9 can move to 8 so
                            there is no need to verify the same.
    '''   
    def __init__(self,item=0,key=0,size=0):
        self.item = item
        self.key = key
        self.size = size
        self.localList = {0:None,1:None,2:None,3:None,4:None}
        self.neighbour = {'UP':None,'DOWN':None,'LEFT':None,'RIGHT':None}
        self.ignore_flags ={'UP':False,'DOWN':False,'LEFT':False,'RIGHT':False}
        self.rank = rank
        self.factor = factor
    '''
    SET-OBJECTS:
        Sets neighbours with item_class objects.
    '''   
        
    def setObjects(self,data):
        rowCurrent = self.key[0]
        colCurrent = self.key[1]   
        rowS = self.size[1]-1
        colS = self.size[0]-1        
        up   = (rowCurrent - 1,colCurrent) 
        down = (rowCurrent + 1,colCurrent)
        right= (rowCurrent,colCurrent + 1)
        left = (rowCurrent,colCurrent - 1)
              
        if rowCurrent == 0 and colCurrent == 0: #(0,0)
             self.neighbour['DOWN'] = data[down]
             self.neighbour['RIGHT'] = data[right]
        elif rowCurrent == 0 and colCurrent == colS: #(0,C)
             self.neighbour['DOWN'] = data[down]
             self.neighbour['LEFT'] = data[left]
        elif rowCurrent == rowS and colCurrent == colS:#(R,C)
             self.neighbour['LEFT'] = data[left]
             self.neighbour['UP'] = data[up]
        elif rowCurrent == rowS and colCurrent == 0:#(R,0)
             self.neighbour['UP'] = data[up]
             self.neighbour['RIGHT'] = data[right]
        elif rowCurrent == 0:#(0,N) WHERE N > 0
             self.neighbour['DOWN'] = data[down]
             self.neighbour['LEFT'] = data[left]
             self.neighbour['RIGHT'] = data[right]
        elif colCurrent == 0: #(N,0) WHERE N > 0
             self.neighbour['UP'] = data[up]
             self.neighbour['DOWN'] = data[down]
             self.neighbour['RIGHT'] = data[right]
        elif colCurrent == colS: #(N,C) WHERE N > 0
             self.neighbour['UP'] = data[up]
             self.neighbour['DOWN'] = data[down]
             self.neighbour['LEFT'] = data[left]
        elif rowCurrent == rowS: #(R,N) WHERE N > 0
             self.neighbour['UP'] = data[up]
             self.neighbour['LEFT'] = data[left]
             self.neighbour['RIGHT'] = data[right]
        else: #(N,M) WHERE N > 0 and N < R and M > 0 and M < C 
             self.neighbour['UP'] = data[up]
             self.neighbour['DOWN'] = data[down]
             self.neighbour['LEFT'] = data[left]
             self.neighbour['RIGHT'] = data[right]
    '''
    CHECK:
        Checks neighbours and removes the one's where the item cannot move for e.g 
        in 8,5,9,2 if we select 5, we identify we cannot move to 9 so we remove it
        from the neighbours and update the ignore_flags of 9
    '''  
    def check(self):
        up =self.neighbour['UP']
        down =self.neighbour['DOWN']
        left =self.neighbour['LEFT']
        right =self.neighbour['RIGHT']
        
        if up is not None:
            if not self.ignore_flags['UP']:
                if up.getItem() > self.item:
                    self.neighbour['UP'] = None
                    up.ignoreFlags('d')
                else:
                    up.getNeighbour()['DOWN'] = None
                    self.ignoreFlags('u')
                            
        if down is not None:
            if not self.ignore_flags['DOWN']:
                if down.getItem() > self.item:
                    self.neighbour['DOWN'] = None
                    down.ignoreFlags('u')
                else:
                    down.getNeighbour()['UP'] = None
                    self.ignoreFlags('d')
                               
        if left is not None:
            if not self.ignore_flags['LEFT']:
                if left.getItem() > self.item:
                    self.neighbour['LEFT'] = None
                    left.ignoreFlags('r')
                else:
                    left.getNeighbour()['RIGHT'] = None
                    self.ignoreFlags('l')
                               
        if right is not None:
            if not self.ignore_flags['RIGHT']:
                if right.getItem() > self.item:
                    self.neighbour['RIGHT'] = None
                    right.ignoreFlags('l')
                else:
                    right.getNeighbour()['LEFT'] = None
                    self.ignoreFlags('r')
   
    def getItem(self):
        return self.item
    
    def getNeighbour(self):
        return self.neighbour
    
    def ignoreFlags(self,flag):      
        if flag is 'd':
            self.ignore_flags['DOWN'] = True          
        if flag is 'u':
            self.ignore_flags['UP'] = True          
        if flag is 'l':
            self.ignore_flags['LEFT'] = True        
        if flag is 'r':
            self.ignore_flags['RIGHT'] = True
            
    '''
    EXPAND and EXPAND-DIRECTED:
        1.Traverses through Matrix according to the allowed neighbours to find
        the best possible path. 
        2.Saves a local copy of path for reference at every step , so that if 
        any other item traverses through that same step, it can access the path for 
        that step without having to traverse again.
        3.Only save a prefreable path with best prospects discards all others.
    '''         
    def expand(self):           
        if self.localList[0] is not None:
            return self.localList[0] 
        elif self.localList[1] is not None:
             return self.localList[1]
        elif self.localList[2] is not None:
            return self.localList[2]         
        elif self.localList[3] is not None:
            return self.localList[3]
        elif self.localList[4] is not None:
            return self.localList[4]
                
        up =self.neighbour['UP']
        down =self.neighbour['DOWN']
        left =self.neighbour['LEFT']
        right =self.neighbour['RIGHT']
        upoint,dpoint,lpoint,rpoint = (0,0),(0,0),(0,0),(0,0)
        upList,downList,leftList,rightList = None,None,None,None        
        compare = 0
        
        if up is not None:
           upList = self.expandDirected(up)
           upoint = self.rank(upList)
           selectedList = upList
           selected = 0
           compare +=1
        if down is not None:
           downList = self.expandDirected(down)
           dpoint = self.rank(downList)
           selectedList = downList
           selected = 1
           compare +=1
        if left is not None:
           leftList = self.expandDirected(left)
           lpoint = self.rank(leftList)
           selectedList = leftList        
           selected = 2
           compare +=1           
        if right is not None:
           rightList = self.expandDirected(right)
           rpoint = self.rank(rightList)
           selectedList = rightList          
           selected = 3
           compare +=1
           
        if compare > 1:
            selected = self.factor(upoint,dpoint,lpoint,rpoint) 
            selectedList = upList if selected == 0\
            else downList if selected == 1\
            else leftList if selected==2\
            else rightList if selected == 3\
            else ()
        elif compare == 0:
            selectedList = (self.item,)  
            selected = 4
        
        self.localList[selected] = selectedList
        return selectedList
    
    def expandDirected(self,obj):
       currentList = (self.item,)
       expandedList = obj.expand()
       currentList = currentList + expandedList
       return currentList
               
data ={}
pathData ={}
row =0
'''
OPENS FILE:
   Reads the file and generate item class objects by passing numbers from
   the file
'''       
with open("map.txt") as f:
    for line in f:
       dataList = list(map(int, line.split()))
       if(len(dataList) == 2):
           rowSize = dataList[0]
           colSize = dataList[1]
       else:    
           dataList = list((item_class(item=item,key=(row,col),size=(rowSize,colSize)) for col,item in enumerate(dataList)))
           data.update({(row,col):value for col,value in enumerate(dataList)})
           row+=1
'''
GENERATE-NEIGHBOUR:
   Generates neighbours of the item/number
'''  
def generateNeighbour():
    for key, value in data.items():
        value.setObjects(data)
        value.check()
'''
Expand-Path:
   Expands path and calculates rank and factor to find the best path.
'''  
def expand_path():
    top_tuple = (0,0)    
    top_key = (0,0)
    for key, value in data.items():
        value=value.expand()
        if not len(value) <= 1:
            pathData[key] = value
            current_tuple = rank(value)
            if not factor(top_tuple,current_tuple) == 0:
                top_tuple = current_tuple
                top_key = key
        
    print(top_tuple)
    print(pathData[top_key])

       
generateNeighbour()
expand_path()



